package hoapm.myshopee.handle.functional;

public interface ThrowingFunction<T, R> {
    R apply(T t) throws Exception;
}
