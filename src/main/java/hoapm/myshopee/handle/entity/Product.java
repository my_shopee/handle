package hoapm.myshopee.handle.entity;

import hoapm.myshopee.handle.dto.ProductStatus;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "_product")
@Data
public class Product extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Long price;
    @Column
    private String name;
    @Column
    private ProductStatus status;
    @Column
    private Integer numberInStock;

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        return id != null && id.equals(((Product) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
