package hoapm.myshopee.handle.entity;

import hoapm.myshopee.handle.dto.ProductStatus;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "_category")
@Data
public class Category extends Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String name;
    @OneToMany(mappedBy = "_product", cascade = CascadeType.ALL,  orphanRemoval = true)
    private List<Product> products = new ArrayList<>();

    public void addProduct(Product product) {
        products.add(product);
        product.setCategory(this);
    }
    public void removeProduct(Product product) {
        products.remove(product);
        product.setCategory(null);
    }
}
